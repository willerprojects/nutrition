﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DTINutri.domain
{
    /// <summary>
    /// Classe de alimentos selecionados para uma respectiva consulta
    /// </summary>
    public class FoodConsultation
    {

        [Key]
        public int FoodConsultationId { get; set; }
        [ForeignKey("Consultation"), Column(Order = 0)]
        public int ConsultationId { get; set; }

        [ForeignKey("Food"), Column(Order = 1)]
        public int FoodId { get; set; }
        
        public virtual Food Food { get; set; }
        public virtual Consultation Consultation { get; set; }

        public FoodConsultation() { }

    }
}