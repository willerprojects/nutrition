﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DTINutri.domain
{
    /// <summary>
    /// Classe de consulta de paciente
    /// </summary>
    public class Consultation
    {
        [Key]
        public int ConsultationId { get; set; }
        public DateTime DateHour { get; set; }
        public double Weight { get; set; }
        public double FatPercent { get; set; }
        public String PhysicalSensation { get; set; }
        public String FoodRestriction { get; set; }
        public int TargetConsumption { get; set; }
        public string Status { get; set; }
        public int PatientId { get; set; }

        public virtual Patient Patient { get; set; }

        public Consultation()
        {
        }

      
      
    }
}