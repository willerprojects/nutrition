﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DTINutri.domain
{
    /// <summary>
    /// Classe de alimentos
    /// </summary>
    public class Food
    {
        [Key]
        public int FoodId { get; set; }
        public string Name { get; set; }
        public double CaloricAmount { get; set; }
        public int FoodGroupId { get; set; }
        public virtual FoodGroup FoodGroup { get; set; }

        public Food() { }

    }
}