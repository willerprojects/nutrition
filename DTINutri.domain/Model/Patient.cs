﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DTINutri.domain
{
    /// <summary>
    /// Classe de pacientes
    /// </summary>
    public class Patient
    {
        [Key]
        public int PatientId { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
        public String HomePhone { get; set; }
        public String CellPhone { get; set; }
        public String Email { get; set; }
        public DateTime Birthday { get; set; }

        public virtual ICollection<Consultation> Consultations { get; set; }

        public Patient() { }
    }
}