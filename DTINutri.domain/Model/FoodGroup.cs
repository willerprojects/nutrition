﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DTINutri.domain
{
    /// <summary>
    /// Classe de grupos alimentares
    /// </summary>
    public class FoodGroup
    {
        public int FoodGroupId { get; set; }
        public String Name { get; set; }
        public FoodGroup() { }

        public virtual ICollection<Food> Foods { get; set; }
    }
}