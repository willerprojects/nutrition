﻿using DTINutri.data.Maps;
using DTINutri.domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTINutri.data
{
    public class NutriContext : DbContext
    {
        //Realiza a criação do contexto
        public NutriContext() : base("name=NutriContext")
        {

        }

        public DbSet<Patient> Patients { get; set; }
        public DbSet<Food> Foods { get; set; }
        public DbSet<FoodConsultation> FoodConsultations { get; set; }
        public DbSet<FoodGroup> FoodGroups { get; set; }
        public DbSet<Consultation> Consultations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new FoodMap());
            modelBuilder.Configurations.Add(new FoodConsultationMap());
            modelBuilder.Configurations.Add(new FoodGroupMap());
            modelBuilder.Configurations.Add(new PatientMap());
            modelBuilder.Configurations.Add(new ConsultationMap());

            base.OnModelCreating(modelBuilder);
        }

    }
}
