﻿using DTINutri.domain;
using System.Data.Entity.ModelConfiguration;

namespace DTINutri.data.Maps
{
    public class FoodGroupMap : EntityTypeConfiguration<FoodGroup>
    {
        public FoodGroupMap()
        {
            this.ToTable("FoodGroup");
            this.HasKey(x => x.FoodGroupId);

            //propriedades
            this.Property(x => x.FoodGroupId).HasColumnName("FoodGroupId");
            this.Property(x => x.Name).HasColumnName("Name");

        }
    }
}
