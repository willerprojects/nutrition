﻿using DTINutri.domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTINutri.data.Maps
{
    public class FoodConsultationMap : EntityTypeConfiguration<FoodConsultation>
    {
        public FoodConsultationMap()
        {
            this.ToTable("FoodConsultation");
            this.HasKey(x => x.FoodConsultationId);

            //propriedades
            this.Property(x => x.FoodConsultationId).HasColumnName("FoodConsultationId");
            this.Property(x => x.FoodId).HasColumnName("FoodId");
            this.Property(x => x.ConsultationId).HasColumnName("ConsultationId");
            

        }
    }
}
