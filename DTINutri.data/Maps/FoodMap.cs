﻿using DTINutri.domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTINutri.data.Maps
{
    public class FoodMap : EntityTypeConfiguration<Food>
    {
        public FoodMap()
        {
            this.ToTable("Food");
            this.HasKey(x => x.FoodId);

            //propriedades
            this.Property(x => x.FoodId).HasColumnName("FoodId");
            this.Property(x => x.Name).HasColumnName("Name");
            this.Property(x => x.CaloricAmount).HasColumnName("CaloricAmount");
            
            HasRequired(c => c.FoodGroup).WithMany(b => b.Foods).HasForeignKey(b => b.FoodGroupId);
        }
    }
}