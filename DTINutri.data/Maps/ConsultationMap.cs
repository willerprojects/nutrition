﻿using DTINutri.domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTINutri.data.Maps
{
    public class ConsultationMap : EntityTypeConfiguration<Consultation>
    {
        public ConsultationMap()
        {
            this.ToTable("Consultation");
            this.HasKey(x => x.ConsultationId);

            //propriedades
            this.Property(x => x.ConsultationId).HasColumnName("ConsultationId");
            this.Property(x => x.PatientId).HasColumnName("PatientId");
            this.Property(x => x.FatPercent).HasColumnName("FatPercent");
            this.Property(x => x.FoodRestriction).HasColumnName("FoodRestriction");
            this.Property(x => x.PhysicalSensation).HasColumnName("PhysicalSensation");
            this.Property(x => x.Status).HasColumnName("Status");
            this.Property(x => x.TargetConsumption).HasColumnName("TargetConsumption");
            this.Property(x => x.Weight).HasColumnName("Weight");
            this.Property(x => x.DateHour).HasColumnName("DateHour");

            HasRequired(c => c.Patient).WithMany(b => b.Consultations).HasForeignKey(b => b.PatientId);

        }
    }
}
