﻿using DTINutri.domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTINutri.data.Maps
{
    public class PatientMap : EntityTypeConfiguration<Patient>
    {
        public PatientMap()
        {
            this.ToTable("Patient");
            this.HasKey(x => x.PatientId);

            //propriedades
            this.Property(x => x.PatientId).HasColumnName("PatientId");
            this.Property(x => x.Name).HasColumnName("Name");
            this.Property(x => x.Address).HasColumnName("Address");
            this.Property(x => x.HomePhone).HasColumnName("HomePhone");
            this.Property(x => x.CellPhone).HasColumnName("CellPhone");
            this.Property(x => x.Email).HasColumnName("Email");
            this.Property(x => x.Birthday).HasColumnName("Birthday");
        }
    }
}
