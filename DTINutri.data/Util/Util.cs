﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTINutri.data
{
    public static  class Util
    {
        //Get your key from config file to open the lock!
        private static string arquivoLog = @"C:\Temp\log.txt";
        public static void gravarLogErro(String nomeFuncao, String exceptionText)
        {
            try
            {
                //para gravar em um arquivo texto
                using (StreamWriter writeLog = new StreamWriter(arquivoLog, true))
                {
                    //composição do que será escrito no arquivo
                    writeLog.WriteLine("ERROR : " + nomeFuncao + string.Format("----------{0} -----------", DateTime.Now.ToString()));
                    writeLog.WriteLine(exceptionText);
                    writeLog.WriteLine();
                    //finaliza whitelog
                    writeLog.Dispose();
                }
            }
            catch (Exception ex)
            {
                //If error write in console (to tests)
                Console.WriteLine("ERRO AO GRAVAR LOG: " + ex.Message);
            }
        }

   
    }
}
