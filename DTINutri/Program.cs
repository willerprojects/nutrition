﻿using DTINutri.Controllers;
using DTINutri.domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DTINutri
{
    public static class Program
    {
        static void Main(string[] args)
        {
        menu:
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine("**********************************************************************");
            Console.WriteLine("**************************** DTI NUTRITION ***************************");
            Console.WriteLine("**********************************************************************");
            Console.WriteLine();
            Console.WriteLine("Nutricionista: " + "Dra. Mariana");
            Console.WriteLine();
            Console.WriteLine("**********************************************************************");

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Digite o número correspondente para acesso ao menu:");
            Console.WriteLine();
            Console.WriteLine("**********************************************************************");
            Console.WriteLine("MENU");
            Console.WriteLine();
            Console.WriteLine("1 - Cadastro Paciente");
            Console.WriteLine("2 - Listar Pacientes");
            Console.WriteLine("3 - Agendar Consulta");
            Console.WriteLine("4 - Cadastro Grupo Alimentar");
            Console.WriteLine("5 - Cadastro Alimento");
            Console.WriteLine("6 - Listar Alimentos");
            Console.WriteLine("7 - Listar Grupos Alimentares");
            Console.WriteLine("8 - Listar Agendamentos");
            Console.WriteLine("9 - Ajuda");
            Console.WriteLine();
            Console.WriteLine("Informe 0 para SAIR do sistema.");
            Console.WriteLine();
            Console.WriteLine("**********************************************************************");


            string input = Console.ReadKey().KeyChar.ToString();

            switch (input)
            {
                //Verifica encerramento do programa
                case "0":
                    Console.WriteLine(" Deseja encerrar o programa? S/N");
                exit:
                    //Verifica entrada do usuário
                    string exit = Console.ReadKey().KeyChar.ToString();
                    if (exit.ToUpper() == "S")
                    {
                        Console.WriteLine(" Saindo do sistema...");
                        //Determina o tempo para delay de saída
                        Thread.Sleep(3000);
                        //Finaliza programa
                        Environment.Exit(0);
                    }
                    else if (exit.ToUpper() == "N")
                    {
                        goto menu;
                    }
                    else
                    {
                        Console.WriteLine(" Informe S para Confirmar e N para Cancelar");
                        goto exit;
                    }

                    break;
                    //Realiza o cadastro do paciente conforme os parâmetros necessários
                case "1":
                    Console.WriteLine(" Cadastro de Paciente");
                    Console.WriteLine();
                    try
                    {
                        //Cria objeto de paciente
                        Patient p = new Patient();

                        //Requisita o nome
                        Console.Write("Nome: ");
                    name:
                        p.Name = Console.ReadLine().ToString();
                        if (p.Name == "")
                        {
                            Console.WriteLine("Campo Obrigatório. Informe novamente:");
                            goto name;
                        }

                        //Requisita endereço
                        Console.Write("Endereço: ");
                    address:
                        p.Address = Console.ReadLine().ToString();
                        if (p.Address == "")
                        {
                            Console.WriteLine("Campo Obrigatório. Informe novamente:");
                            goto address;
                        }

                        //Requisita númerlo de celular
                        Console.Write("Celular: ");
                        p.CellPhone = Console.ReadLine().ToString();

                        //Requisita telefone residencial
                        Console.Write("Tel. Residencial: ");

                    homePhone:
                        p.HomePhone = Console.ReadLine().ToString();

                        //Realiza consistência de obrigatoriedade caso não haja celular informado
                        if (p.HomePhone == "" && p.CellPhone == "")
                        {
                            Console.WriteLine("Campo Obrigatório. Informe novamente:");
                            goto homePhone;
                        }

                        //Requisita data de nascimento
                        Console.Write("Nascimento: ");

                        DateTime userDateTime;

                        //Realiza a validação da data de nascimento
                    data:
                        if (DateTime.TryParse(Console.ReadLine(), out userDateTime))
                        {
                            p.Birthday = userDateTime.Date;
                        }
                        else
                        {
                            Console.WriteLine("Data Incorreta. Informe novamente:");
                            goto data;
                        }

                        //Requisita o email do paciente
                        Console.Write("Email: ");
                    email:
                        p.Email = Console.ReadLine().ToString();

                        if (p.Email == "")
                        {
                            Console.WriteLine("Campo Obrigatório. Informe novamente:");
                            goto email;
                        }
                        Console.WriteLine();

                        //Finaliza cadastro do paciente
                        Console.WriteLine("Cadastrando paciente... ");
                        PatientController.RegisterPatient(p);
                        Console.WriteLine("Cadastro de paciente salvo!");
                        Thread.Sleep(3000);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Ocorreu um erro ao realizar o cadastro. Tente novamente!");
                    }

                    goto menu;
                //Realiza a busca dos pacientes cadastratos
                case "2":
                    Console.WriteLine(" Listar Pacientes");
                    Console.WriteLine();

                    //Recupera a lista de pacientes
                    List<Patient> patients = PatientController.ListPatientS();

                    //Verifica se a lista de pacientes contém registros
                    if (patients.Count > 0)
                    {
                        //Formata a saída
                        Console.WriteLine("----------------------------------------");
                        foreach (Patient patient in patients)
                        {
                            Console.WriteLine("ID:              " + patient.PatientId);
                            Console.WriteLine("Nome:            " + patient.Name);
                            Console.WriteLine("Endereço:        " + patient.Address);
                            Console.WriteLine("Celular:         " + patient.CellPhone);
                            Console.WriteLine("Tel Residencial: " + patient.HomePhone);
                            Console.WriteLine("Nascimento:      " + patient.Birthday.ToShortDateString());
                            Console.WriteLine("Email:           " + patient.Email);
                            Console.WriteLine("----------------------------------------");
                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine(" Não há pacientes cadastrados...");
                    }

                    Console.WriteLine(" Aperte ENTER para voltar ao menu...");
                    Console.ReadLine();

                    goto menu;
                //Realiza agendamento de consulta
                case "3":
                    Console.WriteLine(" Agendar Consulta");
                    Console.WriteLine();

                    //Recupera a lista de pacientes
                    List<Patient> listpatients = PatientController.ListPatientS();

                    //Inicia a lista de Ids de pacientes
                    List<int> listIdPatients = new List<int>();

                    //Cria o objeto de consulta
                    Consultation consult = new Consultation();

                    //verifica se a lista de pacientes contém registros
                    if (listpatients.Count > 0)
                    {

                        Console.WriteLine("-----------------------------------------------------");
                        Console.WriteLine(" ID                " + " | " + "         NOME");
                        Console.WriteLine("-----------------------------------------------------");
                        foreach (Patient patient in listpatients)
                        {
                            //Adiciona o id do paciente na lista de IDs.
                            listIdPatients.Add(patient.PatientId);
                            Console.WriteLine(" " + patient.PatientId + "                  | " + patient.Name);
                            Console.WriteLine("-----------------------------------------------------");
                            Console.WriteLine();
                        }

                        //Requisita o identificador do paciente conforme tabela anterior
                        Console.Write("Informe o identificador do paciente: ");
                    idPaciente:
                        ConsoleKeyInfo info = Console.ReadKey();

                        //Verifica se o que foi digitado é um número inteiro e válido
                        if (char.IsDigit(info.KeyChar))
                        {
                            int id = int.Parse(info.KeyChar.ToString());

                            //Verifica se o número digitado contém na lista dos identificadores
                            if (listIdPatients.Contains(id))
                            {
                                consult.PatientId = id;
                            }
                            else
                            {
                                //Caso não esteja na lista, requista novamente
                                Console.WriteLine();
                                Console.WriteLine(" Informe um número válido!");
                                goto idPaciente;
                            }
                        }
                        else
                        {
                            //Caso não seja válido, requisita novamente
                            Console.WriteLine();
                            Console.WriteLine(" Informe um número válido!");
                            goto idPaciente;
                        }

                        Console.WriteLine();

                        //Requisita a restrição alimentar, que é um campo aberto e textual
                        Console.Write("Informe a restrição alimentar: ");
                    restricao:
                        consult.FoodRestriction = Console.ReadLine().ToString();

                        if (consult.FoodRestriction == "")
                        {
                            Console.WriteLine("Campo Obrigatório. Informe novamente:");
                            goto restricao;
                        }

                        Console.WriteLine();


                        //Requisita a informação do peso do paciente
                        Console.Write("Informe o peso do paciente: ");
                    peso:
                        double peso;
                        if (double.TryParse(Console.ReadLine(), out peso))
                        {
                            consult.Weight = peso;
                        }
                        else
                        {
                            Console.WriteLine();
                            Console.WriteLine("Informe um número válido!");
                            goto peso;
                        }


                        Console.WriteLine();

                        //Requisita o percentual de gordura do paciente
                        Console.Write("Informe o percentual de gordura: ");
                    percent:
                        double percent;
                        if (Double.TryParse(Console.ReadLine(), out percent))
                        {
                            consult.FatPercent = percent;
                        }
                        else
                        {
                            //Caso não tenha sido informado um percentual válido, requisita novamente
                            Console.WriteLine();
                            Console.WriteLine("Informe um número válido!");
                            goto percent;
                        }

                        //Requisita a sensação física, que é um campo aberto e textual
                        Console.Write("Informe a sensação física: ");
                    sensation:
                        consult.PhysicalSensation = Console.ReadLine().ToString();

                        if (consult.PhysicalSensation == "")
                        {
                            //Caso não tenha sido informado um valor válido, requisita novamente
                            Console.WriteLine("Campo Obrigatório. Informe novamente:");
                            goto sensation;
                        }


                        Console.WriteLine();

                        //Requisita a meta de consumo calórico, que deve ser passada como campo numérico
                        Console.Write("Informe a meta de consumo calórico: ");
                        double calorias = 0;
                    metaCalorica:
                        double meta;
                        if (double.TryParse(Console.ReadLine(), out meta))
                        {
                            calorias = meta;
                        }
                        else
                        {
                            //Caso não seja um número válido, requisita novamente
                            Console.WriteLine();
                            Console.WriteLine("Informe um número válido!");
                            goto metaCalorica;
                        }

                        //carrega a lista de grupos cadastrados
                        List<FoodGroup> listGroups = FoodGroupController.ListGroups();

                        //Verifica se há 3 grupos cadastrados
                        if (listGroups.Count == 3)
                        {
                            List<Food> listFood_1 = new List<Food>();
                            List<Food> listFood_2 = new List<Food>();
                            List<Food> listFood_3 = new List<Food>();

                            //Preenche as listas dos alimentos conforme os respectivos grupos
                            for (int i = 0; i <= listGroups.Count; i++)
                            {
                                if (listFood_1.Count == 0)
                                {
                                    listFood_1 = FoodController.ListFoodsByGroupId(listGroups[i].FoodGroupId);
                                }
                                else if (listFood_2.Count == 0)
                                {
                                    listFood_2 = FoodController.ListFoodsByGroupId(listGroups[i].FoodGroupId);
                                }
                                else if (listFood_3.Count == 0)
                                {
                                    listFood_3 = FoodController.ListFoodsByGroupId(listGroups[i].FoodGroupId);
                                }
                            }

                            List<Food> listaPlanoGrupo_1 = new List<Food>();
                            List<Food> listaPlanoGrupo2 = new List<Food>();
                            List<Food> listaPlanoGrupo3 = new List<Food>();

                           //Cria o plano cartesiano para verificar as possíveis combinações de alimentos
                            foreach (Food a in listFood_1)
                            {
                                foreach (Food b in listFood_2)
                                {
                                    foreach (Food c in listFood_3)
                                    {
                                        double sum = a.CaloricAmount + b.CaloricAmount + c.CaloricAmount;
                                     
                                        //Caso a soma das calorias dos alimentos seja menor ou igual às calorias 'limites' informadas, adiciona na lista   
                                        if (sum <= calorias)
                                        {
                                            listaPlanoGrupo_1.Add(a);
                                            listaPlanoGrupo2.Add(b);
                                            listaPlanoGrupo3.Add(c);
                                        }

                                    }

                                }
                            }

                            //Verifica se foram adicionados os alimentos na lista passada
                            if (listaPlanoGrupo_1.Count > 0 && listaPlanoGrupo2.Count > 0 && listaPlanoGrupo3.Count > 0)
                            {
                                //Cria na tela a lista de opções
                                List<string> listOpcoes = new List<string>();

                                Console.WriteLine();
                                Console.WriteLine("Opções ");
                                Console.WriteLine();
                                Console.WriteLine("-------------------------------------------------------- ");
                                for (int f = 0; f < listaPlanoGrupo_1.Count; f++)
                                {

                                    listOpcoes.Add(Convert.ToString(f));
                                    double somacalorias = listaPlanoGrupo_1[f].CaloricAmount + listaPlanoGrupo2[f].CaloricAmount + listaPlanoGrupo3[f].CaloricAmount;

                                    Console.WriteLine(" " + f + " - " + listaPlanoGrupo_1[f].Name  + " - " + listaPlanoGrupo2[f].Name + " - " + listaPlanoGrupo3[f].Name  + " ( " + somacalorias + " calorias)");
                                    Console.WriteLine("-------------------------------------------------------- ");
                                }

                                Console.WriteLine();

                                //Requisita a opção a ser utilizada
                                Console.Write("Selecione a opção pelo identificador: ");
                            op:
                                ConsoleKeyInfo infoOp = Console.ReadKey();
                                int opSelecionado = 0;

                                
                                if (char.IsDigit(infoOp.KeyChar))
                                {
                                    int id = int.Parse(infoOp.KeyChar.ToString());

                                    if (listOpcoes.Contains(Convert.ToString(id)))
                                    {
                                        opSelecionado = id;
                                    }
                                    else
                                    {
                                        Console.WriteLine();
                                        Console.WriteLine(" Informe um número válido!");
                                        goto op;
                                    }
                                }
                                //Caso a opção não seja numérica, requisita novamente
                                else
                                {
                                    Console.WriteLine();
                                    Console.Write(" Informe um número válido!");
                                    goto op;
                                }

                                Console.WriteLine();

                                //Requisita a informação da data da consulta
                                Console.Write("Data Consulta: ");

                                DateTime dataConsulta;
                            dataConsulta:
                                if (!DateTime.TryParse(Console.ReadLine(), out dataConsulta))
                                {                                    
                                    Console.Write("Data Incorreta. Informe novamente:");
                                    goto dataConsulta;
                                }
                                else
                                {
                                    //Realiza tratamento de Data
                                    if (dataConsulta < DateTime.Now)
                                    {
                                        Console.Write("Data Incorreta. Informe novamente:");
                                        goto dataConsulta;
                                    }

                                    Console.WriteLine();
                                    Console.Write("Hora Consulta: ");
                                hora:
                                    string hora = Console.ReadLine().ToString();

                                    //Realiza tratamento de hora
                                    TimeSpan hour;

                                    if (TimeSpan.TryParse(hora, out hour))
                                    {
                                        DateTime horaConsulta = new DateTime(dataConsulta.Year, dataConsulta.Month, dataConsulta.Day, hour.Hours, hour.Minutes, hour.Seconds);
                                        consult.DateHour = horaConsulta;
                                    }
                                    else
                                    {
                                        Console.Write("Hora Incorreta. Informe novamente:");
                                        goto hora;
                                    }

                                }
                                

                                consult.Status = "Agendada";

                                Console.WriteLine("Cadastrando consulta... ");

                                //Conclui o agendamento
                                ConsultationController.RegisterConsultation(consult);

                                Console.WriteLine("Cadastro de consulta salvo!");
                                Thread.Sleep(3000);

                            }else
                            {
                                Console.WriteLine("Não há opções de alimentos para a meta selecionada... ");
                                Console.WriteLine("Selecione outra meta:");
                                goto metaCalorica;
                            }

                        }
                        else
                        {
                            Console.WriteLine(" Deve-se realizar o cadastro de 3 grupos alimentares...");
                        }


                    }

                    goto menu;
                //Realiza cadastramento de grupo alimentar
                case "4":
                    Console.WriteLine(" Cadastro Grupo Alimentar");

                    try
                    {
                        //Recupera a lista de grupos
                        List<String> listGroups = FoodGroupController.ListGroups().Select(x => x.Name.ToUpper()).ToList();

                        //Caso a lista de grupos contenha menos de 3 elementos, permite realizar o cadastro
                        if (listGroups.Count < 3)
                        {
                            FoodGroup groupFood = new FoodGroup();

                            //Requisita o nome do grupo
                            Console.Write("Nome: ");
                        name:
                            groupFood.Name = Console.ReadLine().ToString();


                            if (groupFood.Name == "")
                            {
                                Console.WriteLine("Campo Obrigatório. Informe novamente:");
                                goto name;
                            }

                            Console.WriteLine();

                            //Verifica se o nome informado já existe
                            if (listGroups.Contains(groupFood.Name.ToUpper()))
                            {
                                Console.WriteLine("Grupo existente. Cadastre novamente: ");
                                goto name;
                            }
                            else
                            {

                                Console.WriteLine("Cadastrando grupo de alimento... ");
                                FoodGroupController.RegisterFoodGroup(groupFood);
                                Console.WriteLine("Cadastro de grupo alimento salvo!");
                                Thread.Sleep(3000);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Não é possível realizar o cadastro de mais de 3 grupos... ");
                            Console.WriteLine(" Aperte ENTER para voltar ao menu...");
                            Console.ReadLine();
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Ocorreu um erro ao realizar o cadastro. Tente novamente!");
                        Console.ReadLine();
                    }

                    goto menu;
                //Realiza o cadastramento de aliemntos
                case "5":
                    Console.WriteLine(" Cadastro Alimento");

                    try
                    {
                        //Recupera a lista de grupos
                        List<FoodGroup> groups = FoodGroupController.ListGroups();

                        //Verifica se existem grupos cadastrados
                        if (groups.Count() == 0)
                        {
                            Console.WriteLine(" Não há grupos cadastrados...");
                            Console.ReadLine();
                        }
                        else
                        {
                            Food food = new Food();
                            Console.Write("Grupo: ");

                            Console.WriteLine();

                            //lista de id de grupos
                            List<int> listIdGroup = new List<int>();

                            //Exibe os grupos cadastrados em tela
                            Console.WriteLine("-----------------------");
                            foreach (FoodGroup group in groups)
                            {
                                listIdGroup.Add(group.FoodGroupId);
                                Console.WriteLine(group.FoodGroupId + " - " + group.Name);
                                Console.WriteLine();
                            }
                            Console.WriteLine("-----------------------");
                            Console.WriteLine();

                            //Requisita do usuário a opção de grupo conforme tabela anterior
                            Console.WriteLine("Escolha uma opção: ");
                        grup:
                            ConsoleKeyInfo info = Console.ReadKey();

                            //Verifica se o número informado é válido
                            if (char.IsDigit(info.KeyChar))
                            {
                                int id = int.Parse(info.KeyChar.ToString());

                                if (listIdGroup.Contains(id))
                                {
                                    //Após a seleção de grupos requisita o nome do alimento
                                    Console.Write("Nome: ");
                                name:
                                    food.Name = Console.ReadLine().ToString();

                                    if (food.Name == "")
                                    {
                                        Console.WriteLine("Campo Obrigatório. Informe novamente:");
                                        goto name;
                                    }


                                    food.FoodGroupId = id;
                                }
                                else
                                {
                                    Console.WriteLine("Informe um número válido!");
                                    goto grup;
                                }

                            }
                            else
                            {
                                Console.WriteLine("Informe um número válido!");
                                goto grup;
                            }

                            //Requisita as calorias do alimento
                            Console.Write("Calorias: ");
                        calor:
                            double caloriasTotais;
                            if (double.TryParse(Console.ReadLine(), out caloriasTotais))
                            {
                                food.CaloricAmount = caloriasTotais;
                            }
                            else
                            {
                                Console.WriteLine("Informe um número válido!");
                                goto calor;
                            }

                            Console.WriteLine();

                            //Conclui o cadastro do alimento
                            Console.WriteLine("Cadastrando alimento... ");
                            FoodController.RegisterFood(food);
                            Console.WriteLine("Cadastro de alimento salvo!");
                            Thread.Sleep(3000);
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Ocorreu um erro ao realizar o cadastro. Tente novamente!");
                        Console.ReadLine();
                    }


                    goto menu;
                //Realiza listagem dos alimentos
                case "6":
                    Console.WriteLine(" Listar Alimento:");

                    Console.WriteLine();

                    //Recupera a lista de alimentos
                    List<Food> foods = FoodController.ListFoods().OrderBy(x => x.FoodGroupId).ToList();
                                        
                    if (foods.Count() != 0)
                    {

                        //Realiza o preenchimento na tela em formato tabela
                        Console.WriteLine("-----------------------------------------------------------------------");
                        Console.WriteLine(" ID     |     ALIMENTO        |    GRUPO          |     CALORIAS    ");
                        Console.WriteLine("-----------------------------------------------------------------------");
                        foreach (Food food in foods)
                        {
                            Console.WriteLine(" " + food.FoodId + "      |      " + food.Name + "   | " + food.FoodGroup.Name + "              | " + food.CaloricAmount);
                            Console.WriteLine("-----------------------------------------------------------------------");
                        }
                    }
                    else
                    {
                        Console.WriteLine(" Não há alimentos cadastrados...");
                    }

                    Console.WriteLine(" Aperte ENTER para voltar ao menu...");
                    Console.ReadLine();

                    goto menu;
                    //Realiza a listagem de grupos alimentares
                case "7":
                    Console.WriteLine(" Listar Grupos Alimentares");


                    Console.WriteLine();

                    //Recupera a lista de grupos
                    List<FoodGroup> groupsFood = FoodGroupController.ListGroups();

                    if (groupsFood.Count() != 0)
                    {
                        //Exibe a lista de grupos em tela
                        Console.WriteLine("-----------------------");
                        foreach (FoodGroup group in groupsFood)
                        {
                            Console.WriteLine(group.FoodGroupId + " - " + group.Name);
                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine(" Não há grupos alimentares cadastrados...");
                    }

                    Console.WriteLine(" Aperte ENTER para voltar ao menu...");
                    Console.ReadLine();

                    goto menu;
                    //Lista os agendamentos realizados
                case "8":
                    Console.WriteLine(" Listar Agendamentos");
                    Console.WriteLine();

                    //Recupera a lista de consultas/agendamentos
                    List<Consultation> consults = ConsultationController.ListConsultation();

                    if (consults.Count > 0)
                    {
                        //Exibe os agendamentos em tela
                        Console.WriteLine("----------------------------------------");
                        foreach (Consultation consultation in consults)
                        {
                            Console.WriteLine("ID:              " + consultation.ConsultationId);
                            Console.WriteLine("Paciente:        " + consultation.Patient.Name);
                            Console.WriteLine("Telefone:        " + consultation.Patient.CellPhone + " - " + consultation.Patient.HomePhone);
                            Console.WriteLine("Data:            " + consultation.DateHour);
                            Console.WriteLine("Status:          " + consultation.Status);
                            Console.WriteLine("----------------------------------------");
                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine(" Não há consultas agendadas...");
                    }

                    Console.WriteLine(" Aperte ENTER para voltar ao menu...");
                    Console.ReadLine();

                    goto menu;
                    //Cria o menu ajuda para auxiliar nas informações do sistema
                case "9":
                    Console.WriteLine(" Ajuda");
                    Console.WriteLine();

                    Console.WriteLine("Para utilizar o sistema, você deve realizar as seguintes ordens de ");
                    Console.WriteLine("cadastro:");
                    Console.WriteLine();
                    Console.WriteLine("1 - Prepare a base do sistema cadastrando o Grupo Alimentar através ");
                    Console.WriteLine("    da OPÇÃO 4 do menu.");
                    Console.WriteLine();
                    Console.WriteLine("2 - Cadastre os alimentos de acordo com seus grupos alimentares através ");
                    Console.WriteLine("    da OPÇÃO 5 do menu.");
                    Console.WriteLine();
                    Console.WriteLine("3 - Após realizar estes cadastros, utilize a OPÇÃO 1 para cadastrar ");
                    Console.WriteLine("    os pacientes.");
                    Console.WriteLine();
                    Console.WriteLine("4 - Para realizar um agendamento de paciente, utilize a OPÇÃO 3.");
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine(" Aperte ENTER para voltar ao menu...");
                    Console.ReadLine();

                    goto menu;

                default:
                    Console.WriteLine(" Opção inexistente. Selecione outra opção!");
                    goto menu;
            }


        }


    }

    
}
