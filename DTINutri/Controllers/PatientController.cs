﻿using DTINutri.data;
using DTINutri.domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTINutri.Controllers
{
    public class PatientController
    {
        /// <summary>
        /// Método controller para registro de paciente
        /// </summary>
        /// <param name="patient"></param>
        public static void RegisterPatient(Patient patient)
        {
            using (var db = new NutriContext())
            {
                db.Patients.Add(patient);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Método controller para listagem de pacientes
        /// </summary>
        /// <returns></returns>
        public static List<Patient>  ListPatientS()
        {
            using (var db = new NutriContext())
            {
               return db.Patients.ToList();
            }
        }

    }
}
