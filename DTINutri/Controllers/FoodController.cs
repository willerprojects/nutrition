﻿using DTINutri.data;
using DTINutri.domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTINutri.Controllers
{
    public class FoodController
    {
        /// <summary>
        /// Método controller de cadastro de alimento
        /// </summary>
        /// <param name="food"></param>
        public static void RegisterFood(Food food)
        {
            using (var db = new NutriContext())
            {
                db.Foods.Add(food);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Método controller de listagem de alimentos incluindo os grupos
        /// </summary>
        /// <returns></returns>
        public static List<Food> ListFoods()
        {
            using (var db = new NutriContext())
            {
                return db.Foods.Include("FoodGroup").ToList();
            }
        }

        /// <summary>
        /// Método controller oara listagem de alimentos pelo id do grupo
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public static List<Food> ListFoodsByGroupId(int group)
        {
            using (var db = new NutriContext())
            {
                return db.Foods.Where(x => x.FoodGroupId == group).ToList();
            }
        }
    }


}
