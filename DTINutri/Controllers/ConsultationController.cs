﻿using DTINutri.data;
using DTINutri.domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTINutri.Controllers
{
    class ConsultationController
    {
        /// <summary>
        /// Método controller para realizar o registro de consulta do paciente
        /// </summary>
        /// <param name="consultation"></param>
        public static void RegisterConsultation(Consultation consultation)
        {
            using (var db = new NutriContext())
            {
                db.Consultations.Add(consultation);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Método Controller para realizar a listagem de consultas incluindo as informações do paciente
        /// </summary>
        /// <returns></returns>
        public static List<Consultation> ListConsultation()
        {
            using (var db = new NutriContext())
            {
                return db.Consultations.Include("Patient").ToList();
            }
        }

    }
}
