﻿using DTINutri.data;
using DTINutri.domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTINutri.Controllers
{
    public class FoodGroupController
    {
        /// <summary>
        /// Método controller para registro de grupo de alimento
        /// </summary>
        /// <param name="group"></param>
        public static void RegisterFoodGroup(FoodGroup group)
        {
            using (var db = new NutriContext())
            {
                db.FoodGroups.Add(group);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Método controller para listagem de grupos
        /// </summary>
        /// <returns></returns>
        public static List<FoodGroup> ListGroups()
        {
            using (var db = new NutriContext())
            {
                return db.FoodGroups.ToList();
            }
        }

    }
}
